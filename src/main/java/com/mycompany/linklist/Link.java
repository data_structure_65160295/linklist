/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linklist;

/**
 *
 * @author informatics
 */
public class Link {

    public int iData; // data item (key)
    public double dData; // data item
    public Link next;

    public Link(int id, double dd) // constructor
    {
        iData = id; // initialize data
        dData = dd; // (‘next’ is automatically
    } // set to null)
// -------------------------------------------------------------

    public void displayLink() // display ourself
    {
        System.out.print("{" + iData + ", " + dData + "} ");
    }
} // end class Link
////////////////////////////////////////////////////////////////



